#!/bin/bash
# Check latest kernel on Ubuntu public repository

inst_kernelO=$(uname -r)
inst_kernelN="${inst_kernelO//-*/}"
ubuntu_repo="http://kernel.ubuntu.com/~kernel-ppa/mainline"
comment="CURRENT"
linux_getarch="amd64"
linux_gethead=0
destdir="/tmp/kernel.ubuntu.com"
rc="false"
wget_log="wget_${RANDOM}.log"
install=0
keep_debs=0
find=0

help() {
    cat <<EOF | expand -t 4

List, download and install latest kernel from Ubuntu public repository

    Usage: bash ${0} {-x|-d <version>|-f <pattern>} [-a <arch>] [-h|-H] [-p <path>] [-r] [-i] [-k]

    -x              check latest versions
    -f <pattern>    find version on repository, regex accepted
    -d <version>    download linux kernel (use "latest" for latest version)
    -a <arch>       CPU architecture, default is "${linux_getarch}"
    -h              download headers also
    -H              download headers only
    -p <path>       download location, default is "${destdir}"
    -r              include RC kernels in list and download
    -i              install downloaded debs
    -k              keep downloaded debs after install

EOF
}

filter() {
    grep -A 1000 -B 3 ${inst_kernelN} |\
    if [[ "${1}" != "true" ]] ; then
        sed '/-rc[0-9]*/d'
    else
        tee
    fi
}

check_xml() {
    declare xmlpath
    xmlpath="html/body/table/tr/td/a"

    curl -s ${ubuntu_repo}/ \
    | xml_grep --html --text_only ${xmlpath} \
    | sed "/^v/!d; s/^v//g; s/\/$//g; s/${inst_kernelN}/${inst_kernelN}\t${comment}/g;" \
    | sort -V |\
    if [[ ${find} = 0 ]] ; then
        filter ${rc}
    else
        tee
    fi
}

get_linux() {
    declare linux_getver
    declare down_cmd
    declare remote_path
    declare image_opt
    declare headall_opt
    declare headgen_opt
    linux_getver="${1}"
    if [[ "${linux_getver}" = "latest" ]] ; then
        linux_getver=$( check_xml | awk 'END{print $1}' )
        echo "Chosen version: ${linux_getver}"
    fi
    down_cmd="wget -nv -r -np -nd -l1 -R index.html* -e robots=off -P ${destdir} -a ${destdir}/${wget_log}"
    remote_path=" ${ubuntu_repo}/v${linux_getver}/ "
    image_opt=" --accept-regex image.*generic.*${linux_getarch}\.deb "
    headall_opt=" --accept-regex headers.*all\.deb "
    headgen_opt=" --accept-regex headers.*generic.*${linux_getarch}\.deb "

    mkdir -p ${destdir}

    if [[ "${linux_gethead}" -le 1 ]] ; then
        echo "Downloading kernel image for ${linux_getver} ..."
        ${down_cmd} ${image_opt} ${remote_path}
    fi

    if [[ "${linux_gethead}" -ge 1 ]] ; then
        echo "Downloading kernel headers for ${linux_getver} ..."
        ${down_cmd} ${headall_opt} ${remote_path}
        ${down_cmd} ${headgen_opt} ${remote_path}
    fi

    if [[ "${install}" = 1 ]] ; then
        echo "Installing debs ..."
        declare -a ilist
        ilist=$( grep -o '".*\.deb\.*[0-9]*"' ${destdir}/${wget_log} | sed 's/\"//g' )
        sudo dpkg -i ${ilist[@]}
        if [[ "${keep_debs}" = 0 ]] ; then
            rm ${ilist[@]}
            rm "${destdir}/${wget_log}"
        fi
    fi
}

find_linux() {
    declare linux_getver
    linux_getver="${1}"
    check_xml | grep "${linux_getver}"
}

if [[ "${#}" = 0 ]] ; then
    help
fi

while getopts ":xd:f:a:hHp:rik" OPT ; do
    case ${OPT} in
        x)  command="check_xml" ;;
        d)  command="get_linux ${OPTARG}" ;;
        f)  find=1 ; command="find_linux ${OPTARG}" ;;
        a)  linux_getarch="${OPTARG}" ;;
        h)  linux_gethead=1 ;;
        H)  linux_gethead=2 ;;
        p)  destdir="${OPTARG}" ;;
        r)  rc="true" ;;
        i)  install=1 ;;
        k)  keep_debs=1 ;;
        \?)
            echo -e "\nInvalid option: -${OPTARG}" 1>&2
            help | grep -B1 ' Usage:'
            exit 1
            ;;
        :)
            echo -e "\nArgument required for option: -${OPTARG}" 1>&2
            help | grep -B1 ' Usage:'
            exit 1
            ;;
    esac
done

$command
