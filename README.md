# ubu_late_kern

List, download and install latest Linux kernel from Ubuntu public repository

### Dependencies
- cURL
- xml_grep (xml-twig-tools)
- sudo

### Usage
    Usage: bash ubu_late_kern.sh {-x|-d <version>|-f <pattern>} [-a <arch>] [-h|-H] [-p <path>] [-r] [-i] [-k]

    -x              check latest versions
    -f <pattern>    find version on repository, regex accepted
    -d <version>    download linux kernel (use "latest" for latest version)
    -a <arch>       CPU architecture, default is "amd64"
    -h              download headers also
    -H              download headers only
    -p <path>       download location, default is "/tmp/kernel.ubuntu.com"
    -r              include RC kernels in list and download
    -i              install downloaded debs
    -k              keep downloaded debs after install

##### Examples
List

	$ bash ubu_late_kern.sh -x
    4.7.2
    4.7.3
    4.7.4
    4.7.5	CURRENT
    4.7.6
    4.8

Download

    $ bash ubu_late_kern.sh -d 4.8 -h -p Downloads/linux-kernel/4.8
    Downloading kernel image for 4.8 ...
    Downloading kernel headers for 4.8 ...
